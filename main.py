def main():
    name = 'Ernest'
    statement = 'Hello World'
    print(f'{name}: {statement}')


if __name__ == '__main__':
    main()
